# Instância EC2
resource "aws_instance" "example" {
  ami                         = var.instance_ami
  instance_type               = var.instance_type
  subnet_id                   = aws_subnet.example.id
  vpc_security_group_ids      = [aws_security_group.example.id]
  key_name                    = var.aws_SSH_name
  count                       = var.instance_count
  associate_public_ip_address = true
  tags = {
    Name = "ExampleInstance ${count.index}"
  }
}

# Security Group
resource "aws_security_group" "example" {
  name        = "example"
  description = "Example security group"
  vpc_id      = aws_vpc.example.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

 ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

 ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

output "aws_instance_ip_addr" {
  value = aws_instance.example[0].public_ip
}

