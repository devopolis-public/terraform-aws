terraform {
  backend "s3" {
    region = "us-east-1"
    bucket = "terraform-aula"
    key    = "terraform.tfstate"
  }
}

# Provedor AWS
provider "aws" {
  region = var.aws_region
}


