# Terraform-AWS

fonte: https://registry.terraform.io/namespaces/hashicorp

# O que vamos fazer?

Resumindo: um projeto no Gitlab, utilizando terraform para criar os recursos na Aws como por exemplo {Instância Ec2, Security group, Vpc, Bucket S3 ...e etc} , utilizando uma aplicação com docker , Ansible para configurar a Ec2, docker e etc ...e tudo isso através de um pipeline no gitlabci.

### Bora Começar

# - Dia 1

Antes de começar, certifique de ter uma máquina pronta para isso, 
minha recomendação é , um Linux, ou uma maquina virtual Linux , como por exemplo um virtualbox

- mas e as configurações?
depende muito do que vai deixar em sua máquina linux
geralmente eu utilizo linux server, sem interface visual, se for este o caso, 
cpu=1, memória = 3GB , disco= 20Gb

# 1. Instalando o Terraform

Terraform é uma ferramenta de Infraestrutura como Código (IaC) da HashiCorp. Para instalá-lo no Linux, siga as etapas abaixo:

1. 1 - Primeiro, é necessário baixar o pacote de instalação. Você pode fazer isso a partir do terminal com o comando `wget` ou `curl`. Primeiro, visite o site de downloads do Terraform (https://www.terraform.io/downloads.html) para encontrar o link para a versão mais recente. Substitua o link na etapa abaixo pelo link apropriado.

Exemplo para Ubuntu: 

- Baixe o Arquivo com o comando abaixo:

```bash
wget -O- https://apt.releases.hashicorp.com/gpg | sudo gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg
```

- Adicione no source list, com o comando abaixo: 

```bash
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
```

- realize o update e instale o terraform


```bash
sudo apt update && sudo apt install terraform
```

1. 2 - Por fim, você pode verificar se a instalação foi bem-sucedida verificando a versão do Terraform:

```bash
terraform version
```

obs: após o comando vai aparecer algo como o exemplo abaixo.

Terraform v1.4.6
on linux_amd64

Essas instruções assumem que você está usando uma distribuição do Linux que tem os comandos `wget` ou `curl`, `unzip` e `sudo` disponíveis e que você tem permissões de superusuário (root).


# 2. Criando Recursos na AWS com Terraform

Aqui está um exemplo simples de como você pode usar o Terraform para criar uma instância EC2 na AWS.

2. 1 -  **Criar uma pasta para o seu projeto Terraform:**

Primeiro, você vai querer criar um novo diretório para conter todos os arquivos relacionados ao seu projeto Terraform.

```bash
mkdir Projeto-IAC
cd Projeto-IAC
```

2. 2 -  **Configurar o provedor AWS:**

O Terraform usa o conceito de "provedores" para interagir com serviços de nuvem, como a AWS. Você precisará criar um arquivo de configuração do provedor. Para este exemplo, vamos chamá-lo de `main.tf`.

Abra o arquivo `main.tf` em um editor de texto e adicione o seguinte:

```hcl
provider "aws" {
  region = "us-east-1"  # substitua pela região desejada
  # access_key = "your_access_key"
  # secret_key = "your_secret_key"
}
```

Ponto 1: Observe que as credenciais de acesso foram comentadas. É recomendado fornecer as credenciais da AWS através da configuração do AWS CLI ou variáveis de ambiente, e não diretamente no código, para segurança. Se ainda não tiver feito, instale o AWS CLI e configure suas credenciais com `aws configure`.


Ponto 2: se você está realizando em sua conta pessoal, talvez não tenha problema utilizar a conta root, mas essa ação não é recomendada, principalmente se esta realizando no ambiente corporativo, sempre crie um usuário para essa função administrativa e use as chaves deste usuário

2. 3 **Definir o recurso EC2:**

Em seguida, você definirá o recurso EC2 que deseja criar. Adicione o seguinte ao arquivo `main.tf`:

```hcl
resource "aws_instance" "example" {
  ami           = "ami-0c94855ba95c574c8"  # substitua pelo ID da AMI desejada
  instance_type = "t2.micro"

  tags = {
    Name = "ExampleInstance"
  }
}
```

Esta configuração cria uma instância EC2 usando a AMI e o tipo de instância especificados. A tag `Name` é definida como "ExampleInstance". Substitua o ID da AMI conforme necessário. O ID da AMI especificado aqui é para o Amazon Linux 2 LTS na região us-west-2, mas isso pode variar dependendo da região e do tipo de imagem que você deseja usar.

- **Inicializar e aplicar a configuração Terraform:**

Finalmente, você pode inicializar o Terraform e aplicar a configuração. No terminal, na pasta do seu projeto, execute:

```bash
terraform init
```

Isso inicializa o Terraform, baixa o provedor da AWS e prepara tudo para a aplicação. Em seguida, aplique a configuração:

```bash
terraform apply
```

O Terraform exibirá um plano de execução e solicitará que você o aprove. Digite `yes` para continuar e criar a instância EC2.

Por último, lembre-se de que é uma boa prática de segurança não armazenar suas chaves de acesso AWS diretamente nos arquivos de configuração do Terraform. Você pode usar perfis do AWS CLI, variáveis de ambiente ou um backend de segredos, como o AWS Secrets Manager ou HashiCorp Vault, para gerenciar suas chaves de forma segura.

Claro, abaixo está um exemplo de como criar uma VPC (Virtual Private Cloud) na AWS usando o Terraform. Lembre-se de substituir os valores conforme necessário.

# - Dia 2

# 3.  Tente Sozinho(a)

3. 1 - Criar Bucket S3

O Amazon S3 é um serviço de armazenamento de objetos que oferece escalabilidade, disponibilidade de dados, segurança e desempenho. Aqui está um exemplo de como criar um bucket S3 com o Terraform:

Agora vamos tentar sozinho criar um com as configurações abaixo: 

recurso = "aws_s3_bucket" 
bucket = "my-bucket"
acl = "private" 

- Ponto de atenção 1: Acl (depreciado) ( pesquisar)

3. 2 -  Criar Security Group 

Os security group atuam como um firewall virtual para sua instância para controlar o tráfego de entrada e saída. Aqui está um exemplo de como criar um grupo de segurança com o Terraform:

Agora vamos tentar sozinho criar um com as configurações abaixo: 

Recurso = aws_security_group
name        = "example"
description = "Example security group"
liberar ingress port = "22"


- obs: As Respostas estarão no repositório




---

# 4 - Tente Sozinho

4. 1 -  Definir o recurso VPC

A Amazon Virtual Private Cloud (VPC) é um serviço que permite que você lance recursos da AWS em uma rede virtual que você define. Esta rede virtual é logicamente isolada do restante da infraestrutura da AWS, proporcionando um ambiente de rede seguro e personalizável para os seus recursos.

A VPC permite que você controle seu ambiente de rede, incluindo a seleção do intervalo de endereços IP, a criação de sub-redes, a configuração de tabelas de roteamento e gateways de rede.

- então crie com os seguintes dados 
Recurso = aws_vpc , example   
Range= 10.0.0.0/16

4. 2 - Definir a Sub-Rede

Uma divisão de uma VPC. Você pode lançar recursos da AWS em uma sub-rede especificada.

- então crie com os seguintes dados 
recurso = aws_subnet, example
range = 10.0.1.0/24


4. 3 - Definir o internet Gateway

Uma porta de saída e entrada para a Internet.

- então crie com os seguintes dados 
recurso = aws_internet_gateway example 
vpc_id = aws_vpc.example.id


4. 4 - Definir a tabela de rotas

Define as regras para o tráfego de rede dentro da VPC.

recurso =  aws_route_table 
vpc_id = aws_vpc.example.id
cidr_block = 0.0.0.0/0
gateway_id = aws_internet_gateway.example.id

4. 5 - Associar a tabela de rotas à sub-rede 

recurso =  aws_route_table_association" 
subnet_id      = aws_subnet.example.id
route_table_id = aws_route_table.example.id


- Obs: as Respostas estarão no repositório


---

5.   - Tente Sozinho (a)

Observe que alguns recursos das tarefas anteriores (4.1 a 4.5) já estão integrados e outros não, tente identificar quais precisam dessa integração e faça você mesmo essa integração




6. Tente Sozinho (a)

bora organizar a casa? então vamos criar os seguintes arquivos e separar esse código enorme em cada arquivo.

obs: mas antes , guarde o arquivo main.tf com as integrações em uma pasta separada. 

main.tf, network.tf, buckets.tf, Instances.tf, secret.tf 


# - Dia 3

7. Tente Sozinho (a)

E se eu precisar criar mais instancias Aws ? bora pesquisar como fazer isso

obs: você observou que a cada criação de recurso na Aws você recebe um email? fique atento a isso

- Atenção : se você resolver criar 10 instancias , e sem querer colocar 100 , seu dinheiro vai começar a voar , fique esperto (a) rs.



8. Tente sozinho (a)

agora é hora de criar as variáveis e organizar ainda mais , o ambiente esta crescendo e começa a ficar bem dificil precisar abrir arquivos diferentes para realizar alterações, o terraform permite criar um arquivo de variaveis para isso, bora la? 


9. Tente Sozinho (a)

não podemos esquecer de configurar nossa chave ssh para acessar o servidor linux, então crie uma nova chave pem na AWS, salve em algum lugar seguro, e crie o apontamento através do terraform, após isso , só acessar a instância atraves de ssh com a nova chave criada, através do seu terminal linux local:) ... bora lá !! 


- Pegadinha, deixei você quebrando a cabeça com instância apenas com ip privado, bora lá habilitar para que as instancias recebam ip publico :)


10. Tente Sozinho (a)

nosso terraform.state está local, hora de colocar isso no S3 para que fique seguro, 
bora la?  

obs: crie um novo bucket manualmente e aponte o terraform.state para esse novo bucket

# Dia 5


11. Tente Sozinho (a)

divída técnica 1 
- chave Exposta

ou seja, precisamos primeiro: 

A. Download e instalação do Aws cli
B. Configurar o awscli com a sua key
C. Remover as chaves do arquivo do terraform
D. testar com terraform apply
E. finalmente subir o código para o git


Obs: fazer um fork para seu gitlab

---

---



# Dia 6 - AWS


# 1. VPC

A VPC, ou Virtual Private Cloud, da Amazon Web Services (AWS), é um serviço que oferece uma rede virtual privada e isolada na nuvem. Essa rede pode ser personalizada pelo usuário que a define, incluindo seleção de sua própria gama de endereços IP, criação de sub-redes e configuração de tabelas de roteamento e gateways de rede.

Em termos mais simples, a VPC permite que você tenha um pedaço controlável e seguro da infraestrutura da AWS, isolado do restante da nuvem pública AWS. Você pode então usar esta VPC para hospedar seus recursos da AWS, como instâncias EC2 (Elastic Compute Cloud), bancos de dados RDS (Relational Database Service), etc.

### As VPCs oferecem vários benefícios de segurança e conectividade, como:

- A. Segurança através de Grupos de Segurança e Listas de Controle de Acesso à Rede (NACLs) que funcionam como um firewall para controlar o tráfego de entrada e saída.

- B. Conectividade com a sua rede corporativa através de VPNs (Virtual Private Network) ou uma conexão Direct Connect para um data center local.

- C. Sub-redes públicas e privadas para gerenciar quais recursos têm acesso direto à Internet e quais não têm.

- D. Serviços de gateway como gateways de Internet (IGW) para acesso à Internet e gateways NAT (Network Address Translation) para permitir que instâncias privadas acessem a Internet para atualizações e downloads, mantendo-as seguras.

- E. A VPC permite também implementar arquiteturas de nuvem híbridas.

- F. Recursos como VPC Peering, que permite a comunicação entre diferentes VPCs, seja na mesma conta AWS ou em diferentes contas. 

- G. Endpoints de VPC e Gateways de endpoint de serviço privado para conectar sua VPC a serviços compatíveis na AWS sem necessidade de um gateway de internet, conexão VPN, ou AWS Direct Connect.

Em resumo, a VPC da AWS permite que você crie uma versão virtual de uma rede tradicional que você controla dentro do ambiente da AWS, oferecendo a você uma grande quantidade de flexibilidade e controle sobre seus recursos na nuvem.

![Imagem](imagens/vpc.png)

### Subnet Pública ou Privada ?

- na subnet publica é onde deixamos por exemplo uma instância Web, ou qualquer outro serviço que necessite estar dísponivel para acesso ao público externo, ou seja, fora da rede da AWS, na internet. 
uma subrede pública é toda aquela que tem um Internet Gateway atachado, uma vez atachado eu crio uma conexão , uma ponte para a internet; claro que para que a comunicação funcione eu preciso atachar ip publico , configurar meu firewall para entrada de dados, configurar tabela de roteamento, mas o IGW é a peça principal antes de configurar tudo isso;

- na subrede Privada é onde reservamos para Instâncias ou serviços que não podem ser acessadas diretamente pela internet, o que podemos configurar é um nat gateway para que as instâncias ou serviços que estão na subrede privada possam acessar a internet para fazer download de atualizações por exemplo, mas jamais o contrário.

obs: se precisar que instâncias ou serviços se comuniquem na mesma Vpc , mas em AZ diferente , será necessário criar outra subrede, cada subrede reside em apenas uma AZ.

### Tabela de Roteamento

Uma tabela de roteamento é um conjunto de regras, frequentemente em forma de tabela, que determina onde e como o tráfego de dados será direcionado dentro de uma rede. Essas tabelas são usadas por roteadores e switches para direcionar o tráfego interno e externo para o destino apropriado.

Na Amazon Web Services (AWS), cada Virtual Private Cloud (VPC) tem uma tabela de roteamento que controla o tráfego de rede para e entre sub-redes dentro da VPC. Cada sub-rede em sua VPC deve ser associada a uma tabela de roteamento; a tabela de roteamento controla o tráfego de e para as instâncias naquela sub-rede. Uma sub-rede pode ser associada a apenas uma tabela de roteamento de cada vez, mas você pode associar várias sub-redes à mesma tabela de roteamento.

As tabelas de roteamento da AWS consistem em um conjunto de regras (rotas) que são usadas para determinar onde o tráfego de rede precisa ser direcionado. Cada rota na tabela de roteamento consiste em um destino e um alvo:

- O "destino" é o intervalo de endereços IP para o qual a rota se aplica.
- O "alvo" é onde o tráfego para esse intervalo de endereços IP será direcionado.

Por exemplo, uma tabela de roteamento pode ter uma rota que direciona todo o tráfego destinado à Internet (destino 0.0.0.0/0) para um Internet Gateway (IGW) (o "alvo").

toda sub-rede deve ser associada a uma tabela de rotas.

Uma subrede só pode ser ser associada a uma unica tabela de rotas por vez, mas varias subredes podem ser associadas a uma tabela de rota por vez.

a tabela de rotas é criada quando a vpc é criada.

a tabela de rotas que permite a comunicação entre todas as subredes da vpc 

multiplas tabelas de roteamento podem existir em uma vpc

Você pode configurar várias tabelas de roteamento em uma VPC, dependendo das suas necessidades. Isso permite que você configure uma arquitetura de rede complexa e segura com sub-redes públicas e privadas. Por exemplo, você pode ter uma tabela de roteamento para suas sub-redes públicas que direciona o tráfego para um Internet Gateway, enquanto suas sub-redes privadas podem usar uma tabela de roteamento diferente que não permite o tráfego direto para a Internet.

### 1.1 Tente Sozinho

- crie uma Vpc 

Nome = Vpc-Lab
Range = 10.0.0.0/24, 
locação = padrão, 
adicione uma tag = Lab 

- crie 3 subnet associando a sua vpc criada , cada subnet em AZs separadas

subnet1-Lab = 64 ips 
subnet2-Lab= 64 ips
subnet3-Lab= 128 ips
tag = Lab

- crie a tabela de roteamento para a vpc criada e faça a associação das subnets nesta tabela

Nome = route-lab
tag = Lab



# 2. Security Group

O "Security Group" é um recurso de segurança fornecido pela Amazon Web Services (AWS) que funciona como um firewall virtual para as suas instâncias para controlar o tráfego de entrada e saída.

Os Security Groups atuam no nível da instância, o que significa que cada instância em uma VPC pode ser associada a um ou mais Security Groups. Cada Security Group possui regras que permitem ou negam o tráfego de rede específico para as instâncias associadas a ele.

Existem regras separadas para o tráfego de entrada (inbound) e de saída (outbound):

- As regras de entrada controlam o tráfego de rede que está sendo direcionado para as instâncias associadas ao Security Group. 

- As regras de saída controlam o tráfego de rede que está saindo das instâncias associadas ao Security Group.

Cada regra em um Security Group permite o tráfego baseado em três propriedades:

- Tipo de protocolo (por exemplo, TCP, UDP, ICMP)
- Intervalo de portas (por exemplo, a porta 80 para o tráfego da web HTTP)
- Origem (para regras de entrada) ou destino (para regras de saída), que pode ser um intervalo de endereços IP ou outro Security Group.

Vale a pena mencionar que as regras dos Security Groups são sempre permissivas; você não pode criar regras que negam o tráfego. Além disso, as alterações nas regras do Security Group (adicionar, remover ou modificar regras) são aplicadas imediatamente às instâncias associadas, permitindo uma gestão de segurança flexível e ágil. 

Os SG são statefull, ou seja , o trafego de retorno é permitido automaticamente.

O SG Default permite todo o tráfego de saída e a comunicação entre instâncias de um mesmo SG

Por fim, é uma boa prática de segurança usar Security Groups junto com as Listas de Controle de Acesso à Rede (NACLs) para adicionar uma camada adicional de segurança à sua VPC na AWS.

### 2.1 - Tente Sozinho(a)

- Crie um novo security-group e associar a vpc do exercicio anterior

name= SG-Lab
Regra de entrada = "porta 22", "origem internet" 
tag = Lab


# 3. Internet GW

Um Internet Gateway (IGW) é um componente que você pode adicionar a sua Virtual Private Cloud (VPC) na Amazon Web Services (AWS) para permitir a comunicação entre os recursos em sua VPC e a Internet. Em outras palavras, é uma espécie de "ponte" que conecta a sua rede privada virtual à Internet.

Um Internet Gateway executa duas funções principais:

- Fornece um ponto de entrada e saída para o tráfego da Internet para a sua VPC. Isso significa que qualquer instância (como uma instância EC2) na sua VPC que tenha um endereço IP público pode se comunicar diretamente com a Internet. Isso é útil para aplicações web que precisam ser acessíveis publicamente, por exemplo.

- Executa a tradução de endereços de rede (NAT) para instâncias que foram atribuídas endereços IP públicos. Isso permite que essas instâncias se comuniquem com a Internet, apesar de estarem numa rede privada.

Vale a pena notar que mesmo depois de adicionar um Internet Gateway à sua VPC, não significa que todos os recursos dentro da VPC estarão automaticamente expostos à Internet. Você precisa configurar as suas tabelas de roteamento e regras de segurança (como Grupos de Segurança e Listas de Controle de Acesso à Rede) para controlar o tráfego de entrada e saída para as suas instâncias.

uma nova vpc não vem com um IGW atachado.

Pode ser atachado somente 1 IGW por VPC.

Por último, lembre-se de que, embora um Internet Gateway permita a comunicação com a Internet, ele não estabelece automaticamente conectividade VPN ou oferece outras funcionalidades além das mencionadas. Para outras funcionalidades, a AWS oferece outros serviços, como o Virtual Private Gateway para VPNs e o NAT Gateway para permitir que instâncias em sub-redes privadas acessem a Internet para downloads e atualizações, mantendo-as seguras.


### 3.1 - Tente Sozinho(a)

- crie um internet Gateway e associe a vpc que você criou

Nome = IG-Lab
tag = Lab
---
# Dia 7



# 4. Instância Ec2

A Amazon EC2, ou Amazon Elastic Compute Cloud, é um serviço da Amazon Web Services (AWS) que permite aos usuários alugar capacidade computacional virtual, ou servidores, na nuvem da AWS. Esses servidores virtuais são conhecidos como "instâncias".

Cada instância EC2 representa um servidor virtual privado (VPS) na nuvagem AWS e é usado para executar aplicações em uma grande variedade de casos de uso, desde servidores web e bancos de dados até aplicações empresariais complexas e sistemas de computação de alto desempenho.

As instâncias EC2 vêm em vários tipos e tamanhos para acomodar diferentes cargas de trabalho e requisitos de performance. Por exemplo, alguns tipos de instâncias são otimizados para computação, memória, armazenamento ou desempenho de rede. Além disso, as instâncias EC2 suportam uma variedade de sistemas operacionais, incluindo Amazon Linux, Ubuntu, Windows Server e muitos outros.

As principais características do Amazon EC2 incluem:

- Elasticidade: Você pode aumentar ou diminuir rapidamente a capacidade de acordo com as demandas de sua aplicação, lançando mais instâncias quando necessário e desligando-as quando não são mais necessárias.

- Controle total: Você tem controle completo sobre suas instâncias, incluindo acesso root e a capacidade de interagir com elas como faria com qualquer máquina.

- Integração AWS: EC2 se integra bem com outros serviços AWS, como Amazon S3 para armazenamento de dados, Amazon RDS para bancos de dados, e Amazon VPC para redes virtuais.

- Segurança: As instâncias EC2 podem ser lançadas em grupos de segurança da Amazon VPC que controlam o acesso a cada instância.

- Escalabilidade: Com o Auto Scaling, você pode garantir que tenha o número certo de instâncias EC2 disponíveis para lidar com a carga de trabalho de sua aplicação.

Além disso, a AWS oferece modelos de preços flexíveis para instâncias EC2, incluindo instâncias sob demanda (pague por hora), instâncias reservadas (contrate por 1 ou 3 anos com descontos), instâncias spot (licitação por capacidade extra a um preço reduzido) e instâncias de poupança (para cargas de trabalho com horários específicos).

### 4.1 Tente Sozinho(a)

- através da console crie uma ec2 t2.micro {Amazon Linux ou Ubuntu}

Nome = Server-Lab,
par de chaves = key-Lab,
Associe com o security group criado no exercicio anterior,
habilite o ip publico,
Discos = "1 Disco de 10 GB, SSD gp3", "e um adicional de 20GB, SSD gp3",

exercicio extra: crie uma nova partição com o nome {dados} com o disco de 20GB,
particione e formate

# 5. S3

O Amazon S3, ou Amazon Simple Storage Service, é um serviço de armazenamento de objetos oferecido pela Amazon Web Services (AWS). Ele é projetado para armazenar e recuperar qualquer quantidade de dados, a qualquer momento, de qualquer lugar na web. É uma solução de armazenamento escalável, segura e de alto desempenho.

Os objetos no S3 são armazenados em unidades de armazenamento chamadas "buckets". Um bucket é semelhante a um diretório ou uma pasta e pode conter qualquer número de objetos (ou seja, arquivos). Cada objeto no S3 é acessível através de uma URL única e pode variar em tamanho de 0 bytes até 5 terabytes.

Aqui estão algumas das principais características e benefícios do Amazon S3:

- Durabilidade e Disponibilidade: O S3 fornece uma durabilidade de 99,999999999% (11 noves), o que significa que os dados armazenados são muito seguros. Além disso, dependendo da classe de armazenamento escolhida, o S3 pode fornecer uma disponibilidade de até 99,99%.

- Escalabilidade: O S3 pode armazenar qualquer quantidade de dados e, como tal, é altamente escalável. Isso o torna adequado para uma ampla variedade de usos, desde pequenos sites até aplicativos que necessitam de petabytes de armazenamento.

- Segurança: O S3 oferece vários recursos de segurança, incluindo controle de acesso baseado em políticas, criptografia de dados em repouso e em trânsito, e o suporte para o AWS Identity and Access Management (IAM).

- Classes de Armazenamento: O S3 oferece várias classes de armazenamento, como S3 Standard, S3 Intelligent-Tiering, S3 Standard-IA (Infrequent Access), S3 One Zone-IA, S3 Glacier e S3 Glacier Deep Archive, permitindo otimizar custos com base na frequência de acesso e na necessidade de recuperação dos dados.

- Gestão de dados: O S3 oferece capacidades avançadas de gestão de dados, como políticas de ciclo de vida, que permitem mover automaticamente os dados entre diferentes classes de armazenamento ou até mesmo excluir os dados após um período de tempo especificado.

- Integração com outros serviços AWS: O S3 se integra bem com outros serviços AWS, como Amazon CloudFront para distribuição de conteúdo, AWS Lambda para processamento de dados e Amazon Athena para análise de dados, entre outros.

O S3 é comumente usado para casos de uso como backup e recuperação, arquivamento, análise de big data, distribuição de conteúdo, armazenamento de aplicativos e muito mais.
---
Ao criar um bucket no Amazon S3, você deve escolher uma região onde os dados serão armazenados. Isso é importante por vários motivos:

- Latência de rede: Os dados armazenados mais perto dos usuários ou dos sistemas que acessam esses dados têm tempos de resposta mais rápidos. Portanto, se a maioria de seus usuários estiver na Europa, por exemplo, você pode querer escolher uma região da AWS na Europa para minimizar a latência.

- Custo: Os custos de armazenamento e transferência de dados podem variar dependendo da região. Portanto, pode ser mais econômico escolher uma região com custos mais baixos se ela também atender a outros requisitos, como latência e conformidade.

- Conformidade e privacidade de dados: Dependendo do tipo de dados que você está armazenando e das leis de privacidade do país ou região em que você ou seus usuários estão localizados, pode haver restrições ou requisitos sobre onde os dados podem ser armazenados. Alguns regulamentos, como o GDPR na União Europeia, têm requisitos específicos para a transferência de dados pessoais fora da UE.

- Resiliência e continuidade dos negócios: Ao escolher a região de armazenamento, você também pode considerar a resiliência a desastres naturais ou outros tipos de interrupções. Algumas organizações optam por armazenar seus dados em várias regiões para garantir a continuidade dos negócios em caso de interrupção em uma região.

Essas são algumas das razões pelas quais a configuração da região é um aspecto importante ao criar um bucket no Amazon S3.

Obs: além da opção de Bucket para armazenamento de dados , é possível também habilitar como um serviço Web para páginas estáticas. 

### 5.1 - Tente Sozinho (a)

- Crie um bucket S3

Nome = bucket-lab 
Região da Aws = mesma região da vpc
Acl = desabilitado
configuração de bloqueio = bloquear acesso publico
versionamento de bucket = Ativado
Tag = Lab
criptografia = SSE-S3
chave do bucket = ativada

# Dia 8

# 6.  IAM

AWS IAM, ou Amazon Web Services Identity and Access Management, é um serviço que ajuda um administrador de segurança a controlar quem é autenticado (assinado) e autorizado (tem permissões) para usar recursos da AWS.

O IAM é um componente essencial da AWS, pois permite que os administradores gerenciem de maneira eficaz o acesso aos serviços e recursos da AWS para seus usuários. Com o IAM, você pode criar e gerenciar usuários e grupos da AWS e usar permissões para permitir e negar o acesso deles aos recursos da AWS. 

Isso é especialmente útil em organizações com múltiplos usuários ou sistemas que precisam interagir com a infraestrutura da AWS. Ao usar o IAM, você pode implementar princípios de mínimo privilégio, garantindo que cada usuário ou sistema tenha apenas as permissões necessárias para realizar suas tarefas específicas, melhorando assim a postura de segurança da sua organização.

### 6.1 - Tente sozinho (a)

- Primeiro vamos criar um grupo e adicionar as permissões que esse grupo vai receber, podemos especificar cada permissão nos detalhes, mas em nosso lab vamos dar permissão full para 2 serviços.

Grupo = Grupo-Lab
Permissões = Ec2FullAccess,S3FullAccess

- Agora vamos criar um usuário e adicionar no grupo que criamos no exercicio anterior, com isso ele vai herdar as permissões.

usuário = user-Lab

# 7. Route 53

O Amazon Route 53 é um serviço de DNS (Sistema de Nomes de Domínio) escalável e altamente disponível oferecido pela Amazon Web Services (AWS). O serviço foi desenvolvido para dar aos desenvolvedores e empresas uma maneira extremamente confiável e econômica de direcionar usuários para aplicações de Internet, traduzindo nomes de domínios amigáveis em endereços IP numéricos que máquinas podem entender.

Aqui estão algumas das funcionalidades do Amazon Route 53:

- Registro de Domínio: Route 53 permite que você registre novos domínios e transfira domínios existentes para a AWS.

- Roteamento de DNS: Com o Route 53, você pode direcionar seu tráfego para várias infraestruturas dentro e fora da AWS.

- Verificação de Saúde: O Route 53 fornece verificações de saúde para monitorar a saúde e a eficácia de suas aplicações e servidores.

- Balanceamento de Carga: Com o Route 53, você pode equilibrar o tráfego entre vários servidores ou data centers.

- Integração com a AWS: O Route 53 foi projetado para funcionar com outros serviços da AWS, como o Amazon S3, o EC2 e o CloudFront.

Esses recursos tornam o Amazon Route 53 uma solução poderosa para gerenciar o tráfego de rede para suas aplicações de Internet.

obs: infelizmente não tem como criar exercicios devido aos preços do recurso.

# Dia 9

# 8. CloudFront

Amazon CloudFront é um serviço de rede de entrega de conteúdo (CDN, Content Delivery Network) fornecido pela Amazon Web Services (AWS). Um CDN é uma rede de servidores que entrega conteúdo da web aos usuários com base em sua localização geográfica. 

Quando um usuário solicita conteúdo que é servido através de um CDN, eles são direcionados para o servidor mais próximo de sua localização. Isso reduz a latência - o tempo que leva para o conteúdo ser entregue ao usuário - o que é particularmente importante para o conteúdo em tempo real, como vídeo e áudio, mas também é relevante para websites e conteúdo estático.

Os principais recursos do Amazon CloudFront incluem:

- Desempenho: O CloudFront fornece entrega rápida de conteúdo com baixa latência e alta velocidade de transferência de dados.

- Segurança: O CloudFront é integrado com a AWS Shield, AWS Web Application Firewall e Route 53 para ajudar a proteger contra vários tipos de ataques, como ataques DDoS.

- Integração com a AWS: O CloudFront é profundamente integrado com outros serviços da AWS, como o S3, EC2, ELB, Route 53 e muito mais.

- Escalabilidade: O CloudFront escala automaticamente para lidar com picos de tráfego.

- Customização: Permite que você adicione, remova ou altere comportamentos de cache e origem para atender às necessidades específicas do seu aplicativo.

- Edge locations: A Amazon tem muitos locais de edge ao redor do mundo, o que ajuda a reduzir a latência para os usuários finais.

Em suma, o Amazon CloudFront é uma solução robusta e escalável para entregar conteúdo da web de maneira eficiente e segura aos usuários finais.

obs: infelizmente não tem como criar exercicios devido aos preços do recurso e tempo de testes

# 9. CloudWatch 

Amazon CloudWatch é um serviço de monitoramento e observabilidade oferecido pela Amazon Web Services (AWS). Ele fornece dados e insights acionáveis para monitorar seus aplicativos, responder a mudanças no desempenho do sistema, otimizar a utilização de recursos e obter uma visão unificada da saúde operacional dos recursos e aplicativos da AWS.

Aqui estão algumas das principais características do Amazon CloudWatch:

- Monitoramento de desempenho e operacional: Com o CloudWatch, você pode coletar e acessar todos os seus dados de desempenho e operacionais em um só lugar, desde logs e métricas até eventos e rastreamentos.

- Análise e visualização de dados: O CloudWatch permite que você use métricas automáticas de painéis de controle, gráficos e estatísticas para visualizar e analisar o desempenho e a saúde operacional de seus aplicativos e infraestrutura da AWS.

- Alarmes e notificações: Você pode configurar alarmes no CloudWatch para notificá-lo ou tomar ações automáticas quando suas métricas atingem um limite que você define.

- Alarmes estáticos: Estes são os alarmes tradicionais do CloudWatch, onde você define um limite específico para uma métrica. Por exemplo, você pode definir um alarme para disparar se a utilização da CPU em uma instância do EC2 ultrapassar 80% por um período de tempo especificado. Estes alarmes são úteis quando você conhece o comportamento esperado de suas métricas e deseja ser alertado quando elas desviam desse padrão.

- Detecção de anomalias: Estes são um tipo mais avançado de alarme que usa aprendizado de máquina para estabelecer um modelo do comportamento normal da métrica e, em seguida, dispara um alarme quando os valores reais da métrica desviam significativamente desse modelo. Por exemplo, se a carga de trabalho da sua aplicação é tipicamente mais alta durante as horas de trabalho e mais baixa à noite e nos fins de semana, um alarme de detecção de anomalias pode aprender esse padrão e alertá-lo se a carga de trabalho for inesperadamente alta ou baixa com base no modelo aprendido. Isso pode ser útil para detectar problemas que não seriam pegos por um limite estático simples.

- Logs e insights: O CloudWatch coleta, armazena e analisa logs de seus serviços e aplicativos da AWS, fornecendo insights valiosos para otimizar a performance e a segurança de seus aplicativos.

- Eventos e automação: Com o CloudWatch Events, você pode definir regras que correspondem a eventos específicos em sua infraestrutura da AWS e tomar ações automatizadas em resposta.

Em suma, o Amazon CloudWatch é uma ferramenta poderosa para a gestão de recursos e aplicações na AWS, fornecendo uma visão detalhada do desempenho e da saúde operacional.

### - 9. 1. Tente Sozinho(a)

- Crie um dashboard de Alarme no cloudwatch para Ec2 
tipo de Limite = Estático
nome do Gráfico = Dash.Cpu-Lab
métrica = CPUUtilization
condição de alarme = maior /igual
período de monitoramento =  10 segundos

# Dia 10

# 10. Bastion Host

Um bastion host, é uma máquina especializada na rede que é projetada para suportar o tráfego de entrada público . Ele fornece um ponto de acesso seguro para uma rede privada a partir de uma rede externa, como a Internet.

Os bastion hosts são normalmente configurados para permitir apenas conexões de entrada para serviços específicos, como SSH (Secure Shell) para servidores Linux ou RDP (Remote Desktop Protocol) para servidores Windows. O objetivo é proteger a rede interna limitando a exposição a ataques e fornecendo um único ponto de controle e auditoria.

No contexto da computação em nuvem, um bastion host é frequentemente usado para conectar de forma segura a uma instância virtual privada dentro de uma rede virtual. Por exemplo, na Amazon Web Services (AWS), você pode ter uma instância EC2 (Elastic Compute Cloud) funcionando como um bastion host numa sub-rede pública, que você usa para se conectar de forma segura a outras instâncias EC2 em suas sub-redes privadas.

Os bastion hosts são uma parte importante de uma estratégia de defesa em profundidade, mas devem ser adequadamente protegidos, pois são alvos óbvios para ataques devido à sua exposição à Internet. Isso pode incluir medidas como o endurecimento do sistema operacional, a minimização dos serviços em execução, a configuração de firewalls e a utilização de autenticação multifatorial.


# 11. Alta Disponibilidade, Alta Escalabilidade, Alta Elasticidade, Considerações de Design



### - 11.1 - Alta Disponibilidade: 

- Refere-se à capacidade de um sistema operar continuamente sem interrupções, mesmo em caso de falhas.
- Geralmente alcançada através de redundância - ter múltiplos componentes que podem executar a mesma tarefa.
- Exemplo: Um banco de dados replicado em várias máquinas. Se uma máquina falhar, as outras mantêm o serviço operacional.
- Outro exemplo: Load Balancing, vamos entender melhor

  
### - 11.2. Alta Escalabilidade:

A escalabilidade é a capacidade de um sistema, rede ou processo de lidar com um aumento crescente de carga de trabalho ou sua capacidade de ser ampliado para acomodar esse crescimento. Em outras palavras, se um sistema é escalável, ele deve ser capaz de acomodar mais usuários, mais transações, mais recursos etc, simplesmente adicionando mais recursos, como CPU, RAM ou armazenamento. Isso geralmente é feito em duas formas:

Escalabilidade vertical, também conhecida como "scale-up", que envolve a adição de mais recursos a um único nó na rede (por exemplo, adicionando mais memória ou uma unidade de processamento mais rápida a um servidor).

Escalabilidade horizontal, também conhecida como "scale-out", que envolve a adição de mais nós à rede (por exemplo, adicionando mais servidores para distribuir a carga).

### - 11.3. Alta Elasticidade

Elasticidade: A elasticidade, por outro lado, refere-se à capacidade de um sistema de se adaptar rapidamente às mudanças na demanda, aumentando ou diminuindo os recursos conforme necessário. A elasticidade é um conceito chave na computação em nuvem, pois permite que as empresas paguem apenas pelos recursos que realmente usam. Quando a demanda aumenta, um sistema elástico automaticamente escala para cima para acomodar a demanda adicional. Da mesma forma, quando a demanda diminui, ele automaticamente escala para baixo para economizar custos.

### - 11.4. Exemplos de Recursos AWS 

- ### ELB

O AWS Elastic Load Balancing (ELB) é um serviço oferecido pela Amazon Web Services que automaticamente distribui o tráfego de entrada de aplicações através de múltiplas instâncias Amazon EC2, zonas de disponibilidade, e regiões. Isso aumenta a disponibilidade e a tolerância a falhas de suas aplicações.

Existem três tipos de Elastic Load Balancing que você pode usar:

- Classic Load Balancer: Este é o primeiro tipo de ELB lançado pela AWS. Ele distribui o tráfego entre as instâncias EC2 com base no protocolo e no número da porta.

- Application Load Balancer: Este é um tipo mais avançado de ELB. Ele opera na camada 7 (camada de aplicação) do modelo OSI, o que significa que ele pode direcionar o tráfego para diferentes serviços de back-end com base no conteúdo das solicitações. Ele é comumente usado em aplicações de micro-serviços e aplicações baseadas em contêineres.

- Network Load Balancer: Este é o mais recente tipo de ELB e é projetado para lidar com milhões de solicitações por segundo mantendo baixas latências. Ele opera na camada 4 (camada de transporte) do modelo OSI e é útil para casos de uso em que é necessário um desempenho extremo.

Além disso, os ELBs oferecem recursos como a verificação de integridade das instâncias, a offloading de criptografia SSL/TLS, e a integração com outros serviços da AWS, como o Auto Scaling e o Route 53. Estes recursos, juntamente com a capacidade de distribuir o tráfego, fazem do AWS Elastic Load Balancing uma solução eficaz para balanceamento de carga na AWS.

### Outros Exemplos: 


A Amazon Web Services (AWS) oferece uma série de serviços que são projetados para serem altamente escaláveis, permitindo que você aumente ou diminua a capacidade conforme necessário para atender às demandas de seus aplicativos. Aqui estão alguns desses serviços:

- Amazon EC2 (Elastic Compute Cloud): Este serviço permite que você lance e dimensione instâncias de servidor virtual de acordo com suas necessidades de computação.

- Amazon S3 (Simple Storage Service): Este é um serviço de armazenamento que permite armazenar e recuperar qualquer quantidade de dados a qualquer momento e de qualquer lugar.

- Amazon RDS (Relational Database Service): Este serviço facilita a configuração, operação e escalabilidade de um banco de dados relacional na nuvem.

- Amazon DynamoDB: Este é um banco de dados NoSQL que oferece desempenho em milissegundos de um dígito em qualquer escala.

- Amazon Aurora: É um banco de dados relacional compatível com MySQL e PostgreSQL que combina a velocidade e a disponibilidade dos bancos de dados comerciais de alta qualidade com a simplicidade e a economia dos bancos de dados de código aberto.

- Amazon Elastic MapReduce (EMR): Este serviço permite processar grandes volumes de dados de forma escalável usando ferramentas populares como Spark, Hadoop, Presto e outros.

- AWS Auto Scaling: Este serviço permite que você dimensione automaticamente seus recursos da AWS de acordo com as políticas definidas de escalabilidade e desempenho.

- Amazon SQS (Simple Queue Service): Este é um serviço de fila de mensagens que pode escalar para enviar, armazenar e receber qualquer quantidade de dados entre componentes de software independentes.

- Amazon Kinesis: Este serviço permite a coleta, processamento e análise de dados de streaming em tempo real.

- AWS Lambda: Este serviço permite que você execute seu código sem provisionar ou gerenciar servidores, e você paga apenas pelo tempo de computação que você consome.

Esses serviços permitem que você escale os recursos que você precisa, quando você precisa deles, ajudando a melhorar a eficiência e a capacidade de resposta do seu ambiente de TI.


### - 11.3. - Considerações de Design:

Ao planejar um sistema para alta disponibilidade e escalabilidade, considerações importantes incluem:

- Balanceamento de carga: distribui efetivamente a carga de trabalho entre os recursos do sistema.
- Redundância: garante que existam componentes de backup para assumir em caso de falhas.
- Particionamento: divide o sistema em partes menores e mais gerenciáveis.
- Automação: utiliza tecnologia para gerenciar e controlar processos do sistema, reduzindo a intervenção humana.



# - 12 - Billing

https://calculator.aws/#/estimate

O AWS Calculator, também conhecido como AWS Pricing Calculator, é uma ferramenta fornecida pela Amazon Web Services (AWS) que ajuda os usuários a estimar o custo de utilização dos serviços da AWS.

Esse recurso é útil para entender quanto custará o uso de serviços específicos da AWS em diferentes configurações. O AWS Calculator permite que você escolha entre diferentes serviços da AWS, incluindo, mas não se limitando a, Amazon EC2 (computação), Amazon S3 (armazenamento), Amazon RDS (banco de dados) e muitos outros.

Você pode definir a região, o tempo de uso, as características de desempenho, os recursos adicionais e muito mais para cada serviço. Depois de inserir essas informações, o AWS Calculator fornecerá uma estimativa do custo mensal.

Além disso, o AWS Pricing Calculator também permite que você compare os custos entre diferentes configurações e serviços, o que pode ser útil para otimizar seus gastos com a AWS.


--

# Dia 11

# Ansible

O Ansible é uma plataforma de automação de TI que facilita a configuração de sistemas, o gerenciamento de implantações de software e a orquestração de tarefas mais complexas de gerenciamento de sistemas de TI.

- Conceitos Básicos:**

1. Inventário**: É a lista de hosts (ou nós) onde o Ansible irá executar tarefas. Você pode definir grupos de hosts para organizar melhor o seu inventário.

2. Playbooks**: São os arquivos onde as automações Ansible são definidas. Eles são escritos em YAML. Cada playbook pode conter um ou mais "plays" que, por sua vez, contém uma lista de tarefas.

3. Módulos**: São programas pré-escritos que o Ansible executa em seus nós para realizar alguma tarefa, como instalar um software ou editar um arquivo de configuração.

4. Variáveis**: Permitem que você personalize seus playbooks para diferentes ambientes, hosts ou situações.

5. Fatos**: São informações sobre os sistemas de destino que o Ansible coleta automaticamente antes de executar um playbook.

6. Handlers

Handlers no Ansible são uma forma especial de tarefas que são executadas somente quando notificadas por outras tarefas. 

Uma tarefa notifica um handler quando a tarefa causa uma mudança no estado do sistema. Por exemplo, se você tem uma tarefa para instalar um pacote de software e o pacote foi efetivamente instalado, isso muda o estado do sistema e a tarefa notificará o handler correspondente. 

Os handlers são comumente usados para reiniciar serviços quando as configurações desses serviços são alteradas. Por exemplo, se você alterar a configuração do servidor web Apache, pode notificar um handler para reiniciar o serviço Apache.



### 1. Tente Sozinho(a)

- Exercício 1**: Instalar o Ansible
Instale o Ansible em um sistema Linux Ubuntu e verifique a instalação 
com o comando `ansible --version`.

a saida será algo como:

ansible --version
ansible [core 2.12.10]
  config file = /etc/ansible/ansible.cfg
  configured module search path = ['/home/dielly/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /usr/lib/python3/dist-packages/ansible
  ansible collection location = /home/dielly/.ansible/collections:/usr/share/ansible/collections
  executable location = /usr/bin/ansible
  python version = 3.8.10 (default, Mar 13 2023, 10:26:41) [GCC 9.4.0]
  jinja version = 2.10.1
  libyaml = True

### 2. Tente Sozinho(a)


  - Exercício 2**: Criar um Inventário
Crie um arquivo de inventário simples com um grupo de hosts chamado 'web' com 2 instâncias ec2.


### 3. Tente Sozinho(a)

- Exercício 3**: Ping Command
Use o módulo ping do Ansible para verificar a conectividade com os hosts no seu inventário.
você deve ver uma resposta de "pong" para cada host.


# Dia 12


### 4. Tente Sozinho (a)

- Exercício 4**: Ad-Hoc Commands
Use um comando ad-hoc para listar todos os arquivos no diretório home do usuário no seu host do grupo web.


### 5. Tente Sozinho (a)

- Exercício 5**: Simple Playbook
Escreva um playbook que instale o servidor nginx no seu grupo de hosts ubuntu.
primeiro instale no grupo 1 e depois nos 2 grupos juntos para entender como é o funcionamento.

---

# Dia 16

# Docker

## 01 - Introdução ao Docker ( O que estudar?)

- Definição e uso do Docker
- Comparação entre Docker e máquinas virtuais
- Instalação do Docker
- Entendendo imagens e contêineres Docker
- Executando o primeiro contêiner Docker com o comando `docker run`

### 01. Tente Sozinho (a)

_Exercício 01:_ Instale o Docker no seu sistema e execute o contêiner "hello-world".

_Verifique se a mensagem do "Hello from Docker!" é exibida. Esta mensagem indica que o Docker foi instalado corretamente._

---

## 02-  Imagens Docker (Estudar)

- Como pesquisar imagens no Docker Hub
- Entendendo as tags das imagens Docker
- Baixando imagens com o comando `docker pull`
- Criando sua própria imagem Docker com um Dockerfile
- Comando `docker build` para construir uma imagem a partir de um Dockerfile
- Enviando sua imagem para o Docker Hub com o comando `docker push`


### 02 Tente Sozinho (a)

_Exercício:_ Pesquise a imagem do nginx no Docker Hub, baixe-a e crie uma imagem Docker personalizada com um arquivo de índice HTML personalizado. 


# Dia 17

## 03 - Gerenciamento de Contêineres (Estudar)

- Iniciando e parando contêineres com `docker start` e `docker stop`
- Listando contêineres com `docker ps`
- Executando comandos dentro de contêineres com `docker exec`
- Removendo contêineres com `docker rm`
- Visualizando logs de contêineres com `docker logs`

### 03 - Tente Sozinho (a)


_Exercício:_ Inicie um contêiner a partir da imagem do nginx criada no Dia 16, verifique seu status e pare o contêiner.

---

## 04 - Redes e Volumes Docker

- Introdução às redes Docker
- Criando e gerenciando redes com `docker network`
- Conectando contêineres a redes
- Introdução aos volumes Docker
- Criando e gerenciando volumes com `docker volume`
- Montando volumes em contêineres

### 04 - Tente Sozinho (a)

_Exercício:_ Crie uma rede Docker personalizada, inicie dois contêineres nessa rede e monte um volume para persistir os dados.

---

# Dia 18

##  05 - Docker Compose

- Introdução ao Docker Compose
- Criando um arquivo docker-compose.yml
- Executando múltiplos contêineres com o Docker Compose
- Gerenciamento de serviços com o Docker Compose (iniciar, parar, escalar)
- Limpeza e depuração com Docker Compose

### 05 - Tente Sozinho (a)

_Exercício:_ Crie um arquivo docker-compose.yml para um aplicativo simples de duas camadas: um banco de dados MySQL "mysql:5.7" e um aplicativo web PHP "php:7-apache".

---


# Dia 21

# Gitlabci


## 01 -  Introdução ao GitLab CI/CD (Estudar)
- Conhecendo o GitLab CI/CD e sua importância.
- Exploração dos pipelines de CI/CD.
- O papel dos arquivos .gitlab-ci.yml.
- Construindo seu primeiro pipeline.
- Aprofundamento em jobs e stages.

### 01 - Primeiro Projeto gitlabci
- Crie um novo projeto no gitlabci com o nome que preferir ex: (Lab-Gitlabci), baixe esse projeto para sua maquina local ou maquina virtual
- no seu projeto local crie um arquivo simples na raiz do projeto com o nome .gitlab-ci.yml {obs: não esqueça do ponto}, dentro do arquivo coloque as informações abaixo e faça o push para o gitlab, abra a tela do seu projeto no gitlab, no lado esquerdo tem a opção build -> pipelines , o seu pipeline vai aparecer neste local.
Neste primeiro momento ele apenas vai executar as mensagens de texto, mas já tem a base para iniciar.

  

  stages:
  - test
  - build
  - deploy

job1:
  stage: test
  script:
    - echo "Executando Job1 de Test"

job2:
  stage: build
   script:
    - echo "Executando Job2 de Build"

job3:
  stage: deploy
  script:
    - echo "Executando Job3 de deploy"




## 02  Tente Sozinho (a) 
- execução sequencial, dependência de jobs: no mesmo código adicione a opção needs, para que o Job2 execute apenas quando o Job 1 tiver sucesso, e o mesmo para o Job3 , que execute apenas quando Job2 finalizar.
- execução paralela: outro teste é a necessidade de executar o job2 e job3 após finalizar o job1 , com isso vamos ter 2 jobs em paralelo.


### 03 - Tente Sozinho (a)

Adicione aos seus jobs para executar apenas quando você fizer push para a branch master, crie essa branch master , faça um push para a main , verifique se rodou ( não deveria), e depois faça um push para a branch master e verifique novamente se rodou.

### 04 - Tente Sozinho (a)

- Crie uma pasta "Docker"
- Crie um pipeline que construa uma imagem Docker de um arquivo Dockerfile simples e faça push para um registro Docker.

### 05 - Tente sozinho
- Crie um novo projeto no gitlabci e baixe esse projeto para sua maquina local ou maquina virtual, crie 3 subpastas (terraform) (Ansible) (Docker)
- Crie um código em gitlabci na raiz do projeto dividido por Estágios.

- Estágio 1. Criação de infra com Terraform , criar uma vpc, 2 subnet, 1 internet gateway , 1  instancia ec2 de uma ami especifica, 1 security group liberando a porta 22,80 e 443 para a internet,  com todos serviços associados

obs: adicional para pegar o ip da instancia aws:

Se você está criando mais de uma instância EC2 (ou seja, a variável `count` é maior que 1), seu recurso `aws_instance.example` é, na verdade, uma lista de instâncias, em vez de uma única instância. Então, você precisa ajustar sua declaração de saída para acomodar isso.

Por exemplo, se você quiser emitir o endereço IP público da primeira instância criada, você pode fazer o seguinte:

```hcl
output "aws_instance_ip_addr" {
  value = aws_instance.example[0].public_ip
}
```

Se você quiser emitir os endereços IP públicos de todas as instâncias criadas, você pode fazer isso:

```hcl
output "aws_instance_ip_addr" {
  value = [for i in aws_instance.example : i.public_ip]
}
```

Este último código irá emitir uma lista de endereços IP públicos.

Nota: Tenha em mente que a numeração do índice começa do zero, então `aws_instance.example[0]` se refere à primeira instância.



- Estagio 2. configurar a ec2 utilizando o ansible, atualizando o ubuntu, instalando o docker

obs: agora ainda no gitlab-cy.yml, no job do ansible , (antes) do script você vai colocar o código abaixo:
```
before_script:
    - echo "[webservers]" > hosts
    - cat instance_ip.txt >> hosts
```

- Estagio 3 vai utilizar o docker-compose para subir um docker mysql:5.7 com outro docker php:7-apache com dependencia do mysql:5.7 ( ou outro banco e webserver que preferir)


obs: No GitLab CI/CD, você pode usar as variáveis de ambiente para armazenar as chaves privadas e outras informações sensíveis. As variáveis de ambiente podem ser definidas no nível do projeto, do grupo ou de todo o GitLab, e podem ser protegidas para serem expostas apenas a pipelines executadas em branches protegidos.

Aqui está como você pode fazer isso:

1. Vá para o seu projeto no GitLab.
2. Vá para Settings > CI/CD.
3. Expanda a seção "Variables".
4. Crie uma nova variável. Por exemplo, nomeie-a como `DEPLOY_KEY`. Cole a chave privada no campo "Value".
5. Salve a variável.

Agora você pode referenciar a variável em seu arquivo `.gitlab-ci.yml`. O GitLab substituirá automaticamente a variável pelo seu valor real durante a execução do pipeline.

```yaml
job3:
  stage: docker
  needs: ["job2"]
  script:
    - echo "$DEPLOY_KEY" > deploy_key.pem
    - chmod 600 deploy_key.pem
    - INSTANCE_IP=$(cat ./terraform/Projeto-IAC/instance_ip.txt)
    - ssh -o StrictHostKeyChecking=no -i deploy_key.pem ec2-user@$INSTANCE_IP "sudo curl -L https://github.com/docker/compose/releases/download/1.28.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose"
    - ssh -o StrictHostKeyChecking=no -i deploy_key.pem ec2-user@$INSTANCE_IP "sudo chmod +x /usr/local/bin/docker-compose"
    - scp -o StrictHostKeyChecking=no -i deploy_key.pem docker-compose.yml ec2-user@$INSTANCE_IP:~
    - ssh -o StrictHostKeyChecking=no -i deploy_key.pem ec2-user@$INSTANCE_IP "docker-compose up -d"
```

Desta forma, a chave privada não é exposta e está segura.

Teste a conexão SSH e a Pagina PHP através da internet, se tudo funcionou , esta finalizado !


--- 


Segue abaixo o código completo e explicação !

As chaves SSH e qualquer tipo de credenciais devem ser armazenadas como variáveis de ambiente seguras no GitLab e não no repositório do código.

No exemplo anterior, a variável `DEPLOY_KEY` é onde a chave privada SSH deve ser armazenada. Você pode definir essa variável de ambiente nas configurações do CI/CD do seu projeto no GitLab, da seguinte forma:

1. Navegue até o seu projeto no GitLab.
2. Vá para "Settings" -> "CI / CD".
3. Expanda a seção "Variables".
4. Clique em "Add Variable".
5. Insira "DEPLOY_KEY" como a key.
6. Coloque sua chave privada SSH no campo "Value".
7. Selecione "Mask variable" para evitar que o valor seja exibido nos logs de jobs.
8. Clique em "Add Variable" para salvar.

Agora a variável `DEPLOY_KEY` contém sua chave privada SSH e será substituída pelo seu valor real durante a execução do pipeline. Isso é muito mais seguro do que armazenar a chave diretamente no repositório.

Dessa forma, ao rodar o `job3`, a linha `- echo "$DEPLOY_KEY" > deploy_key.pem` irá gerar um arquivo `deploy_key.pem` com o conteúdo da sua chave privada SSH. Isso permitirá que você se conecte à sua instância EC2 via SSH e execute os comandos necessários.


agora vamos atualizar o arquivo `.gitlab-ci.yml`:


```yaml
stages:
  - terraform
  - ansible 
  - docker

job1:
  image: hashicorp/terraform:latest
  stage: terraform
  script:
    - cd terraform/Projeto-IAC
    - terraform init
    - terraform apply -auto-approve
    - echo $(terraform output -raw aws_instance_ip_addr) > instance_ip.txt
  artifacts:
    paths:
      - instance_ip.txt

job2:
  stage: ansible
  needs: ["job1"]
  before_script:
    - cd ansible
    - echo "[webservers]" > hosts #limpa o hosts e coloca o webserver
    - cat ../terraform/Projeto-IAC/instance_ip.txt >> hosts #apenas adiciona o IP no arquivo
  script:
    - cd ansible
    - ansible-playbook -i hosts playbook.yaml

job3:
  stage: docker
  needs: ["job2"]
  script:
    - echo "$DEPLOY_KEY" > deploy_key.pem
    - chmod 600 deploy_key.pem
    - INSTANCE_IP=$(cat ./terraform/Projeto-IAC/instance_ip.txt)
    - ssh -o StrictHostKeyChecking=no -i deploy_key.pem ec2-user@$INSTANCE_IP "sudo curl -L https://github.com/docker/compose/releases/download/1.28.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose"
    - ssh -o StrictHostKeyChecking=no -i deploy_key.pem ec2-user@$INSTANCE_IP "sudo chmod +x /usr/local/bin/docker-compose"
    - scp -o StrictHostKeyChecking=no -i deploy_key.pem docker-compose.yml ec2-user@$INSTANCE_IP:~
    - ssh -o StrictHostKeyChecking=no -i deploy_key.pem ec2-user@$INSTANCE_IP "docker-compose up -d"
```

Este script agora usa a chave SSH armazenada na variável de ambiente segura `DEPLOY_KEY` para estabelecer uma conexão SSH com a instância EC2. Lembre-se de substituir `ec2-user` pelo nome de usuário apropriado para a sua instância EC2 e também de substituir `docker-compose.yml` pelo caminho correto para o seu arquivo docker-compose. 

Além disso, por favor note que este é um exemplo básico e pode precisar de ajustes para se adequar perfeitamente às suas necessidades específicas e ao seu ambiente.


   
 

